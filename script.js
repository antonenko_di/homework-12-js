const colorButton = document.querySelectorAll(".btn");
let activeButton = null;

document.addEventListener("keydown", (event) => {
    if (activeButton) {
        activeButton.style.backgroundColor = "black";
    }

    const key = event.key.toUpperCase();
    const targetButton = Array.from(colorButton).find(button => button.textContent.toUpperCase() === key);

    if (targetButton) {
        targetButton.style.backgroundColor = "blue";
        activeButton = targetButton;
    }
});
